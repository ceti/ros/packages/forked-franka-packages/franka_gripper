# Franka Gripper Package

Forked juli 2020 and extended with adaption of applied forces.

Original repository: https://github.com/frankaemika/franka_ros/tree/kinetic-devel/franka_gripper

## How to install it

* navigate into the src-folder of your workspace
* create a folder with the name "franka_ros"
* cd into the franka_ros folder
* clone this repository inside the franka_ros folder

## How to modify applied forces

You can do this by the configuration of one parameter in the parameter-server of ROS.
The parameter "tud_grasp_force", which accepts the force in Newton as parameter.
Optimal parameter range is tested from 5 N to 14 N. 
